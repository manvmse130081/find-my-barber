package com.thang.findmybarber.model

import java.io.Serializable

data class Shop(
    val name: String,
    val address: String,
    val distance: Double,
    val rating: Float,
    val ratingNumber: Int,
    val basePrice: Int,
    val imgUrl: String
): Serializable