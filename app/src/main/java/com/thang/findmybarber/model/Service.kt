package com.thang.findmybarber.model

data class Service(
    val serviceId: Int,
    val serviceName: String,
    val servicePrice: Double,
    val serviceDuration: Int,
    val serviceDescription: String
)