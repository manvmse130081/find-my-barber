package com.thang.findmybarber

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.thang.findmybarber.adapter.SalonAdapter
import com.thang.findmybarber.model.Service
import com.thang.findmybarber.model.Shop
import kotlinx.android.synthetic.main.activity_salon.*

class SalonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_salon)

        val salon = intent.extras?.get("salon") as Shop
        salon_name.text = salon.name
        salon_address.text = salon.address
        salon_rating_bar.rating = salon.rating
        salon_rating_number.text = salon.ratingNumber.toString()

        val services = ArrayList<Service>().apply {
            val service = Service(
                0,
                "Nhuộm tóc màu hường quyến rũ",
                180.0,
                60,
                "Nhuộm xong bạn sẽ cảm thấy mình trở thành 1 con người tự tin hơn trước rất nhiều"
            )
            add(service)
            add(service)
            add(service)
            add(service)
            add(service)
        }
        salon_rvc.adapter = SalonAdapter(services)
    }
}
