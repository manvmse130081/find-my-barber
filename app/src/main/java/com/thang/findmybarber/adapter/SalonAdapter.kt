package com.thang.findmybarber.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.model.Service

class SalonAdapter(private val service: ArrayList<Service>) :
    RecyclerView.Adapter<SalonAdapter.SalonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.salon_item, parent, false)
        return SalonViewHolder(view)
    }

    override fun getItemCount(): Int = service.size

    override fun onBindViewHolder(holder: SalonViewHolder, position: Int) {

    }

    class SalonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}