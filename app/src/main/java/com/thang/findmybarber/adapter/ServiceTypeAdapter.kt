package com.thang.findmybarber.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.thang.findmybarber.R
import kotlinx.android.synthetic.main.service_type_cardview.view.*

class ServiceTypeAdapter(val serviceTypes: ArrayList<String>) : Adapter<ServiceTypeAdapter.ServiceTypeViewHolder>() {

    class ServiceTypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceTypeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.service_type_cardview, parent, false)
        return ServiceTypeViewHolder(view)
    }

    override fun getItemCount(): Int = serviceTypes.size

    override fun onBindViewHolder(holder: ServiceTypeViewHolder, position: Int) {
        val serviceType = serviceTypes[position]
        holder.itemView.service_type_name_text.text = serviceType
        //TODO: implement get image from url
        holder.itemView.service_type_image.setImageResource(R.drawable.haircut_icon_sample)
    }
}