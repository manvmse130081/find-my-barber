package com.thang.findmybarber.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.SalonActivity
import com.thang.findmybarber.model.Shop
import kotlinx.android.synthetic.main.shop_card_view.view.*

class ShopAdapter(private val shops: ArrayList<Shop>) :
    RecyclerView.Adapter<ShopAdapter.ShopViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.shop_card_view, parent, false)
        return ShopViewHolder(view)
    }

    override fun getItemCount(): Int = shops.size

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        val shop = shops[position]
        holder.itemView.run {
            shop_name_text.text = shop.name
            shop_address_text.text = shop.address
            shop_distance_text.text = "${shop.distance}km"
            shop_rating_bar.rating = shop.rating
            shop_rating_text.text = "(${shop.ratingNumber})"
            shop_price_text.text = "Từ ${shop.basePrice}đ"
            shop_image.setImageResource(R.drawable.barber_shop_sample)

            setOnClickListener {
                val intent = Intent(context, SalonActivity::class.java)
                intent.putExtra("salon", shop)
                context.startActivity(intent)
            }
        }
    }

    class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}