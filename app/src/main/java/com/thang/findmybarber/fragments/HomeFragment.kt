package com.thang.findmybarber.fragments


import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.thang.findmybarber.R
import com.thang.findmybarber.R.id.top_shop_rcv
import com.thang.findmybarber.adapter.ServiceTypeAdapter
import com.thang.findmybarber.adapter.ShopAdapter
import com.thang.findmybarber.model.Shop
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

/**
 * A simple [Fragment] subclass.
 */
object HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val shops = ArrayList<Shop>().apply {
            val shop = Shop(
                "Shop name",
                "Shop really long address",
                0.5,
                3.5f,
                100,
                150000,
                "https://example.com"
            )
            add(shop)
            add(shop)
            add(shop)
            add(shop)
            add(shop)
            add(shop)
        }

        val serviceTypes = ArrayList<String>().apply {
            add("Hớt tóc")
            add("Gội đầu")
            add("Nhuộm")
            add("Cạo lông")
            add("Hớt tóc")
            add("Gội đầu")
            add("Nhuộm")
            add("Cạo lông")
            add("Hớt tóc")
            add("Gội đầu")
            add("Nhuộm")
            add("Cạo lông")
        }

        view.findViewById<RecyclerView>(R.id.top_shop_rcv).apply {
            setHasFixedSize(true)
            adapter = ShopAdapter(shops)
        }

        view.findViewById<RecyclerView>(R.id.discount_shop_rcv).apply {
            setHasFixedSize(true)
            adapter = ShopAdapter(shops)
        }

        view.findViewById<RecyclerView>(R.id.near_shop_rcv).apply {
            setHasFixedSize(true)
            adapter = ShopAdapter(shops)
        }

        val dividerItemDecoration = object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                outRect.left = 24
            }
        }

        view.findViewById<RecyclerView>(R.id.services_rcv).apply {
            setHasFixedSize(true)
            adapter = ServiceTypeAdapter(serviceTypes)
            addItemDecoration(dividerItemDecoration)
        }

        return view
    }


}
